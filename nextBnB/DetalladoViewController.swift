//
//  DetalladoViewController.swift
//  nextBnB
//
//  Created by G4B0 CU4DR05_C4RD3N4S on 29/04/17.
//  Copyright © 2017 G4B0 CU4DR05_C4RD3N4S. All rights reserved.
//

import UIKit
import SDWebImage

private let reuseIdentifier = "placeImages"

class DetalladoViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpCollectionView(collection: imagesCollectionView)
        setUpCollectionViewLayout()
        setContentoffsetAnimation()
        
        fillData(name: todosLosAlojamientos[cellSelected!].nombre!,
                 type: todosLosAlojamientos[cellSelected!].tipoDeAlojamineto!,
                 pricePerNight: "$" + todosLosAlojamientos[cellSelected!].precioPorNoche! + " " + todosLosAlojamientos[cellSelected!].moneda!,
                 Like: todosLosAlojamientos[cellSelected!].favorito!)
        
        addGestureRecognizer()
    }

    // Variables
    
    var images : [String] = todosLosAlojamientos[cellSelected!].allImagesURLS!
    var startingPointForView = UIScreen.main.bounds.size.width
    
    // IBOutlets
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeType: UILabel!
    @IBOutlet weak var placePricePerNight: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    
    // IBActions
    
    // Functions
    
    func setUpCollectionView (collection : UICollectionView) {
    
        collection.delegate = self
        collection.dataSource = self
        
    }
    
    func setUpCollectionViewLayout () {
    
        let screenSizeWidth = UIScreen.main.bounds.size.width
        let cellSize = CGSize(width: screenSizeWidth, height: 350)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        imagesCollectionView.setCollectionViewLayout(layout, animated: true)
        
        imagesCollectionView.reloadData()
        
    }
    
    func setContentoffsetAnimation () {
    
        DispatchQueue.main.async {
            
            self.imagesCollectionView.contentOffset.x = CGFloat(-self.startingPointForView)
            
            UIView.animate(withDuration: 0.8, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                
                self.imagesCollectionView.contentOffset.x = CGFloat(0)
                
            }, completion: nil)
            
        }
    }
    
    func fillData (name : String, type : String, pricePerNight : String, Like : Bool) {
    
        placeName.text = name
        placeType.text = type
        placePricePerNight.text = pricePerNight
        
        if Like == true {
            likeImage.image = UIImage(named: "like")
        } else {
            likeImage.image = UIImage(named: "noLike")
        }
        
    }
    
    func addGestureRecognizer () {
    
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(likeOrNot(tapGestureRecognizer: )))
        likeImage.isUserInteractionEnabled = true
        likeImage.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    func likeOrNot (tapGestureRecognizer: UITapGestureRecognizer) {
        
        if likeImage.image == UIImage(named: "like") {
            
            // set to not Like
            
            todosLosAlojamientos[cellSelected!].favorito = false
            likeImage.image = UIImage(named: "noLike")
            
            // ---------------------------------------------------------Eliminar de Favoritos
            utilities.removeSpecificPlace(place: todosLosAlojamientos[cellSelected!])
            
        } else {
            
            // set to Like
            
            todosLosAlojamientos[cellSelected!].favorito = true
            likeImage.image = UIImage(named: "like")
            
            // ---------------------------------------------------------Agregar a Favoritos
            if utilities.loadAlojamientos() != nil {
                Favoritos = utilities.loadAlojamientos()!
                Favoritos.append(todosLosAlojamientos[cellSelected!])
                utilities.saveAlojamientosFavoritos(allAlojamientos: Favoritos)
            } else {
                Favoritos.append(todosLosAlojamientos[cellSelected!])
                utilities.saveAlojamientosFavoritos(allAlojamientos: Favoritos)
            }
        }
        
        
    }
    
    // System Functions

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return (todosLosAlojamientos[cellSelected!].allImagesURLS?.count)!
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! detalladoCollectionViewCellImages
    
        cell.placeImageView.contentMode = .scaleAspectFill
        cell.placeImageView.clipsToBounds = true
        let eachImageURL : URL = (NSURL(string: "\(images[indexPath.row])") as URL?)!
        cell.placeImageView.sd_setImage(with: eachImageURL, placeholderImage: UIImage(named: "placeholder"))
        
        return cell
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    
}
