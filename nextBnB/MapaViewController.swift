//
//  SecondViewController.swift
//  nextBnB
//
//  Created by G4B0 CU4DR05_C4RD3N4S on 28/04/17.
//  Copyright © 2017 G4B0 CU4DR05_C4RD3N4S. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class SecondViewController: UIViewController , MKMapViewDelegate , CLLocationManagerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        requestUserLocation()
        setUpMap(map: mapaDeLugares)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Variables
    
    let locationManager = CLLocationManager()
    
    // IBOutlets
    
    @IBOutlet weak var mapaDeLugares: MKMapView!
    
    // IBActions
    
    // Functions
    
    func requestUserLocation () {
    
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return
            
        case .denied, .restricted:
            locationManager.requestWhenInUseAuthorization()
            break
        default:
            locationManager.requestWhenInUseAuthorization()
            break
        }
        
    }
    
    func setUpMap (map : MKMapView) {
    
        map.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
        
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
        }
        
    }
    
    func setRegion () {
    
        mapaDeLugares.setRegion(MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(latitude,
                                                                                              longitude),
                                                                   2000,
                                                                   2000),
                                animated: true)
        
    }
    
    func reverseGeoCoding (location : CLLocation) {
        
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks : [CLPlacemark]?, error : Error?) in
            
            if error != nil { return }
            
            if (placemarks?.count)! > 0 {
                let place = placemarks?[0]
                cityToSearch = place?.locality! ?? "Los Angeles"
            } else { return }
            
        }
        
    }
    
    func addAllThePins (todosLosPins : [alojamiento]) {
    
        for item in todosLosPins {
        
            let pin = PinAnnotation(title: item.nombre!,
                                    subtitle: "$" + item.precioPorNoche! + " " + item.moneda!,
                                    coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(item.latitud!)!,
                                                                       longitude: CLLocationDegrees(item.longitud!)!))
            mapaDeLugares.addAnnotation(pin)
            
        }
        
    }
    
    // System Functions
    
    override func viewDidAppear(_ animated: Bool) {
        setRegion()
        addAllThePins(todosLosPins: todosLosAlojamientos)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
        
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
            
            reverseGeoCoding(location: CLLocation(latitude: latitude, longitude: longitude))
            
        }
        
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("gg ez")
    }
    
    // Inits

}

