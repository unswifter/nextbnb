//
//  FirstViewController.swift
//  nextBnB
//
//  Created by G4B0 CU4DR05_C4RD3N4S on 28/04/17.
//  Copyright © 2017 G4B0 CU4DR05_C4RD3N4S. All rights reserved.
//

import UIKit
import SDWebImage
import  CoreLocation

class FirstViewController: UIViewController , UITableViewDelegate, UITableViewDataSource , CLLocationManagerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupTableView(table: listadoTableView)
        
        requestUserLocation()
        setUpLocationManager()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Variables
    
    let locationManager = CLLocationManager()
    
    // IBOutlets
    
    @IBOutlet weak var listadoTableView: UITableView!
    
    // IBActions
    
    // Functions
    
    func setupTableView (table : UITableView) {
    
        table.delegate = self
        table.dataSource = self
        return
        
    }
    
    func connectToTheApiAndAddData () {
        
        let noDiacritics = cityToSearch.folding(options: .diacriticInsensitive, locale: .current)
        let newCityNoSpaces = noDiacritics.replacingOccurrences(of: " ", with: "%20")
        
        let request = NSMutableURLRequest(url: NSURL(string: APIs.airbnbData + APIs.clientID + "&" + APIs.limit + "30" + "&" + APIs.location + newCityNoSpaces)! as URL)
    
        todosLosAlojamientos.removeAll()
        
        request.httpMethod = "GET"
        
        let sesion = URLSession.shared
        
        let dataTask = sesion.dataTask(with: request as URLRequest) { (data:Data?, response:URLResponse?, error:Error?) in
            
            if error != nil {
                print(error!)
                
                todosLosAlojamientos = utilities.loadAlojamientos()!
                print("Loading the AFK! :D")
                
                DispatchQueue.main.async {
                    
                    self.listadoTableView.reloadData()
                }
                
            } else {
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                    
                    let results = json["search_results"] as? [[String: Any]] ?? []
                    
                    print("Hay \(results.count) Resultados")
                    
                    var name : String?
                    var imagen : String?
                    var miniatura : String?
                    var propiertyType : String?
                    var favorito : Bool?
                    var id : Int?
                    var latitud : Double?
                    var longitud : Double?
                    
                    var tipoDeMoneda : String?
                    var costoPorNoche : Double?
                    
                    var allImagesURLS : [String]?
                    
                    for resultado in results {
                        let listing = resultado["listing"] as? [String:Any]
                        
                        name = listing?["name"] as? String
                        imagen = listing?["picture_url"] as? String
                        miniatura = listing?["thumbnail_url"] as? String
                        propiertyType = listing?["property_type"] as? String
                        id = Int((listing?["id"] as? Double)!)
                        latitud = listing?["lat"] as? Double
                        longitud = listing?["lng"] as? Double
                
                        allImagesURLS = listing?["picture_urls"] as? [String]
                        
                        let pricing = resultado["pricing_quote"] as? [String:Any]
                        
                        tipoDeMoneda = pricing?["listing_currency"] as? String
                        costoPorNoche = pricing?["localized_nightly_price"] as? Double
                        
                        // true or false?
                        
                        let miFavorites = utilities.loadAlojamientos()
                        favorito = false
                        if miFavorites != nil {
                            for each in miFavorites! {
                                if Int(each.id!) == id {
                                    favorito = true
                                }
                            }
                        }
                        
                        
                        let alojamientoVar = alojamiento(nombre: name!, imagenURL: imagen!, miniaturaURL: miniatura!, precioPorNoche: "\(Int(costoPorNoche!))", moneda: tipoDeMoneda!, tipoDeAlojamiento: propiertyType!, favorito: favorito!, id: "\(id!)", longitud: "\(longitud!)", latitud: "\(latitud!)", allimagesURLs: allImagesURLS!)
                        
                        todosLosAlojamientos.append(alojamientoVar)
                        
                        DispatchQueue.main.async {
                            
                            self.listadoTableView.reloadData()
                            
                        }
                    }
                    
                } catch let error as NSError {
                    print(error)
                }
            }
            
        }
        
        dataTask.resume()
        
    }
    
    func setUpLocationManager () {
        
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
        } else {
        
            cityToSearch = "Los Angles"
            latitude = 34.045521
            longitude = -118.244452
            
            self.connectToTheApiAndAddData()
            
        }
        
    }
    
    func requestUserLocation () {
        
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return
            
        case .denied, .restricted:
            locationManager.requestWhenInUseAuthorization()
            break
        default:
            locationManager.requestWhenInUseAuthorization()
            break
        }
        
    }
    
    // System Functions
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // array.count
        return todosLosAlojamientos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sitioCell", for: indexPath) as! ListadoTableViewCell
        
        cell.imageBackground.sd_setImage(with: NSURL(string: todosLosAlojamientos[indexPath.row].imagenURL!)! as URL, placeholderImage: UIImage(named: "placeholder.png"))
        
        cell.typeLabel.text = todosLosAlojamientos[indexPath.row].tipoDeAlojamineto
        
        cell.priceLabel.text = "$\(Int(todosLosAlojamientos[indexPath.row].precioPorNoche!)!.addSpaceSeparator) \(todosLosAlojamientos[indexPath.row].moneda!)"
        
        if todosLosAlojamientos[indexPath.row].favorito == true {
            cell.likeImage.image = UIImage(named: "like")
        } else {
            cell.likeImage.image = UIImage(named: "noLike")
        }
        
        // Set the tag, to see the position in other VC
        cell.imageBackground.tag = indexPath.row
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cellSelected = indexPath.row
        self.performSegue(withIdentifier: "goToDetallado", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = UIScreen.main.bounds.height
        return (height / 2.6)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if cellSelected != nil {
            self.listadoTableView.reloadRows(at: [IndexPath(row: cellSelected!, section: 0)], with: UITableViewRowAnimation.fade)
        } else {
        
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
            
            reverseGeoCoding(location: CLLocation(latitude: latitude, longitude: longitude))
            
            locationManager.stopUpdatingLocation()
            
        } else {
        
            reverseGeoCoding(location: CLLocation(latitude: latitude, longitude: longitude))
            
        }
        
    }
    
    func reverseGeoCoding (location : CLLocation) {
        
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks : [CLPlacemark]?, error : Error?) in
            
            if error != nil {
                self.connectToTheApiAndAddData()
                return
            }
            
            if (placemarks?.count)! > 0 {
                let place = placemarks?[0]
                cityToSearch = place?.locality! ?? "Los Angeles"
                self.connectToTheApiAndAddData()
            } else { return }
            
        }
        
    }
    
    
}


