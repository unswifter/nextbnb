//
//  APIs.swift
//  nextBnB
//
//  Created by G4B0 CU4DR05_C4RD3N4S on 28/04/17.
//  Copyright © 2017 G4B0 CU4DR05_C4RD3N4S. All rights reserved.
//

import Foundation


struct APIs {
    
    static let airbnbData = "https://api.airbnb.com/v2/search_results?"
    static let clientID = "client_id=3092nxybyb0otqw18e8nh5nty"
    static let limit = "_limit="
    static let location = "location="
    
}
