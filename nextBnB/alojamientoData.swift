//
//  alojamientoData.swift
//  nextBnB
//
//  Created by G4B0 CU4DR05_C4RD3N4S on 28/04/17.
//  Copyright © 2017 G4B0 CU4DR05_C4RD3N4S. All rights reserved.
//

import Foundation

class alojamiento : NSObject, NSCoding {
    
    var nombre : String?
    var imagenURL : String?
    var miniaturaURL : String?
    var precioPorNoche : String?
    var moneda : String?
    var tipoDeAlojamineto : String?
    var favorito : Bool?
    var id : String?
    var longitud : String?
    var latitud : String?
    var allImagesURLS : [String]?
    
    init(nombre: String,
         imagenURL : String,
         miniaturaURL : String,
         precioPorNoche : String,
         moneda : String,
         tipoDeAlojamiento : String,
         favorito : Bool,
         id: String,
         longitud : String,
         latitud: String,
         allimagesURLs : [String]) {
        
        self.nombre = nombre
        self.imagenURL = imagenURL
        self.miniaturaURL = miniaturaURL
        self.precioPorNoche = precioPorNoche
        self.moneda = moneda
        self.tipoDeAlojamineto = tipoDeAlojamiento
        self.favorito = favorito
        self.id = id
        self.longitud = longitud
        self.latitud = latitud
        self.allImagesURLS = allimagesURLs
        
    }
    
    // MARK: NSCoding
    
    required init?(coder aDecoder: NSCoder) {
        
        nombre = aDecoder.decodeObject(forKey: "nombre") as? String
        imagenURL = aDecoder.decodeObject(forKey: "imagenURL") as? String
        miniaturaURL = aDecoder.decodeObject(forKey: "miniaturaURL") as? String
        precioPorNoche = aDecoder.decodeObject(forKey: "precioPorNoche") as? String
        moneda = aDecoder.decodeObject(forKey: "moneda") as? String
        tipoDeAlojamineto = aDecoder.decodeObject(forKey: "tipoDeAlojamiento") as? String
        favorito = aDecoder.decodeObject(forKey: "favorito") as? Bool
        id = aDecoder.decodeObject(forKey: "id") as? String
        longitud = aDecoder.decodeObject(forKey: "longitud") as? String
        latitud = aDecoder.decodeObject(forKey: "latitud") as? String
        allImagesURLS = aDecoder.decodeObject(forKey: "allImagesURLS") as? [String]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.nombre, forKey: "nombre")
        aCoder.encode(self.imagenURL, forKey: "imagenURL")
        aCoder.encode(self.precioPorNoche, forKey: "precioPorNoche")
        aCoder.encode(self.moneda, forKey: "moneda")
        aCoder.encode(self.tipoDeAlojamineto, forKey: "tipoDeAlojamiento")
        aCoder.encode(self.favorito, forKey: "favorito")
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.longitud, forKey: "longitud")
        aCoder.encode(self.latitud, forKey: "latitud")
        aCoder.encode(self.allImagesURLS, forKey: "allImagesURLS")
    }
    
    
    
}


