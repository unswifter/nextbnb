//
//  Utilities.swift
//  nextBnB
//
//  Created by G4B0 CU4DR05_C4RD3N4S on 29/04/17.
//  Copyright © 2017 G4B0 CU4DR05_C4RD3N4S. All rights reserved.
//

import Foundation


class utilities {

    private static let alojamientosKey = "AlojamientosKEY"
    
    private static func archiveAloamientos (house : [alojamiento]) -> NSData {
        
        return NSKeyedArchiver.archivedData(withRootObject: house as NSArray) as NSData
        
    }
    
    static func loadAlojamientos () -> [alojamiento]? {
    
        if let unarchivedObject = userDefaults.object(forKey: alojamientosKey) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? [alojamiento]
        }
        
        return nil
        
    }
    
    static func saveAlojamientosFavoritos (allAlojamientos : [alojamiento]?) {
    
        let archivedObject = archiveAloamientos(house: allAlojamientos!)
        userDefaults.set(archivedObject, forKey: alojamientosKey)
        userDefaults.synchronize()
        
    }
    
    static func removeSpecificPlace (place : alojamiento) {
    
        var todosFavoritos = utilities.loadAlojamientos()
        
        var position = 0
        
        for placeIn in todosFavoritos! {
            
            if placeIn.id == place.id {
                todosFavoritos?.remove(at: position)
                saveAlojamientosFavoritos(allAlojamientos: todosFavoritos)
                userDefaults.synchronize()
                return
            } else {
                saveAlojamientosFavoritos(allAlojamientos: todosFavoritos)
                userDefaults.synchronize()
            }
            
            position += 1
        }
        
    }
    
}









