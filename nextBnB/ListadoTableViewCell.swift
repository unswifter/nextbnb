//
//  ListadoTableViewCell.swift
//  nextBnB
//
//  Created by G4B0 CU4DR05_C4RD3N4S on 28/04/17.
//  Copyright © 2017 G4B0 CU4DR05_C4RD3N4S. All rights reserved.
//

import UIKit

class ListadoTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(likeOrNot(tapGestureRecognizer: )))
        likeImage.isUserInteractionEnabled = true
        likeImage.addGestureRecognizer(tapGestureRecognizer)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // IBOutlets
    
    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    
    
    // Functions
    
    func likeOrNot (tapGestureRecognizer: UITapGestureRecognizer) {
        
        if likeImage.image == UIImage(named: "like") {
            
            // set to not Like
            
            todosLosAlojamientos[imageBackground.tag].favorito = false
            likeImage.image = UIImage(named: "noLike")
            
            // ---------------------------------------------------------Eliminar de Favoritos
            utilities.removeSpecificPlace(place: todosLosAlojamientos[imageBackground.tag])
            
        } else {
            
            // set to Like
            
            todosLosAlojamientos[imageBackground.tag].favorito = true
            likeImage.image = UIImage(named: "like")
            
            // ---------------------------------------------------------Agregar a Favoritos
            if utilities.loadAlojamientos() != nil {
                Favoritos = utilities.loadAlojamientos()!
                Favoritos.append(todosLosAlojamientos[imageBackground.tag])
                utilities.saveAlojamientosFavoritos(allAlojamientos: Favoritos)
            } else {
                Favoritos.append(todosLosAlojamientos[imageBackground.tag])
                utilities.saveAlojamientosFavoritos(allAlojamientos: Favoritos)
            }
            
            
        }
        
        
    }
    
    // Must delete ------->
    
    func alojamientosToNSData(alojamiento : [alojamiento]) -> NSData {
        
        return NSKeyedArchiver.archivedData(withRootObject: alojamiento as NSArray) as NSData
    }
    
    func saveIntoFavoritesUserDefaults (toSave: alojamiento) {
        
        var allAlojamientos : [alojamiento] = []
        
        if let data = userDefaults.array(forKey: "data") {
        
            allAlojamientos = data as! [alojamiento]
            allAlojamientos.append(toSave)
            
            
            let todosNSData = alojamientosToNSData(alojamiento: allAlojamientos)
            userDefaults.set(todosNSData, forKey: "data")
            //userDefaults.set(allAlojamientos, forKey: "data")
            
        } else {
        
            allAlojamientos.append(toSave)
            userDefaults.set(allAlojamientos, forKey: "data")
            
        }
        
        print(userDefaults.array(forKey: "data")!)
        
        /*
         
         //To save the string
         let userDefaults = Foundation.UserDefaults.standard
         userDefaults.set( "String", forKey: "Key")
         
         //To retrieve from the key
         let userDefaults = Foundation.UserDefaults.standard
         let value  = userDefaults.string(forKey: "Key")
         print(value)
         
         */
        
    }
    
    func deleteFromFavoritesUserDefaults (toDelete : alojamiento) {
        
        // crear for y revisar todos los ID, despues eliminar el que coincida
        
        
    }
    
    
    
    
    
    
    
    
    
    
}
