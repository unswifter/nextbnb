//
//  pinAnnotation.swift
//  nextBnB
//
//  Created by G4B0 CU4DR05_C4RD3N4S on 30/04/17.
//  Copyright © 2017 G4B0 CU4DR05_C4RD3N4S. All rights reserved.
//

import Foundation
import MapKit

class PinAnnotation: NSObject, MKAnnotation {
    
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    
    init(title : String , subtitle: String, coordinate : CLLocationCoordinate2D) {
        
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        
    }
    
}








