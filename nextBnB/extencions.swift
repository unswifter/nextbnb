//
//  extencions.swift
//  nextBnB
//
//  Created by G4B0 CU4DR05_C4RD3N4S on 28/04/17.
//  Copyright © 2017 G4B0 CU4DR05_C4RD3N4S. All rights reserved.
//

import Foundation
import UIKit


extension Int {

    var addSpaceSeparator : String {
    
        let nf = NumberFormatter()
        nf.groupingSeparator = "."
        nf.numberStyle = NumberFormatter.Style.decimal
        return nf.string(for: self)!
        
    }
    
}


