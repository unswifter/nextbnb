# README #

### NextBnB ###

* versión simplificada de AirBnB Creado por Gabriel Cuadros Cardenas para la empresa NextDots
* 1.0

### Como lo Configuro? ###

* Clone la ultima versión de este repositorio 
* por medio de la consola diríjase a la carpeta del proyecto     
```
#!Terminal

$cd Desktop/NextBnB
```

* instale y actualize los Pods     
```
#!Terminal

$pod install
```

* abra el .xcworkspace no el .xcodeproj 
* compile y disfrute :D

### A quien debo hablarle? ###

* Admin y Dueño de la Repo
* -Nombre: Gabriel Cuadros Cardenas
* -e-mail: gabirelcc@pi-coders.com
* -telefono: +(57)300-478-9385